# #!/usr/bin/env python
# -*- coding: UTF-8 -*-
from __future__ import unicode_literals

import pathlib
import re
import zipfile

directory = pathlib.Path(".")
pattern = "dist|IDE_utils|oxt|Makefile|__pycache__"

with zipfile.ZipFile("dist/oqrcode.oxt", mode="w") as archive:
    for file_path in directory.rglob("**/*"):
        if re.search(pattern, file_path.name) is None:
            archive.write(file_path, arcname=file_path.relative_to(directory))
