# coding: utf-8
from __future__ import unicode_literals

import pyqrcode
import unohelper
import uno
from com.sun.star.task import XJobExecutor
from com.sun.star.awt import Size
from com.sun.star.text.TextContentAnchorType import AT_PARAGRAPH
from com.sun.star.text.WrapTextMode import THROUGHT
from com.sun.star.text.RelOrientation import PRINT_AREA
from com.sun.star.text.HoriOrientation import RIGHT
from com.sun.star.text.VertOrientation import TOP
from com.sun.star.text.WrapTextMode import LEFT
from com.sun.star.awt.MessageBoxType import (
    MESSAGEBOX, INFOBOX, ERRORBOX, WARNINGBOX, QUERYBOX)

# -----------------------------------------------------------
#   GENERICS
# -----------------------------------------------------------

# uno implementation
g_ImplementationHelper = unohelper.ImplementationHelper()

# options pour pyqrcode
# voir https://pythonhosted.org/PyQRCode/moddoc.html pour une liste complète
SCALE = 10
QUIET_ZONE = 2
ERROR = "Q"

# taille de l'image
IMAGE_SIZE = Size(2000, 2000)

# -----------------------------------------------------------
#   TOOLS
# -----------------------------------------------------------


def create_qrcode(file, txt):
    '''
    Création du QR code
    '''
    url = pyqrcode.create(txt, error=ERROR, encoding="utf-8")
    url.svg(file[8:], scale=SCALE, quiet_zone=QUIET_ZONE,
            xmldecl=False, svgns=False)
    return url


def createUnoService(service, ctx=None, args=None):
    '''
    Instanciate a Uno service. https://gitlab.com/jmzambon/apso

    @service: name of the service to be instanciated.
    @ctx: the context if required.
    @args: the arguments when needed.
    '''
    if not ctx:
        ctx = uno.getComponentContext()
    smgr = ctx.getServiceManager()
    if ctx and args:
        return smgr.createInstanceWithArgumentsAndContext(service, args, ctx)
    elif args:
        return smgr.createInstanceWithArguments(service, args)
    elif ctx:
        return smgr.createInstanceWithContext(service, ctx)
    else:
        return smgr.createInstance(service)


def msgbox(message, title="Message", boxtype='message', buttons=1, win=None):
    '''
    Simple message box. https://gitlab.com/jmzambon/apso

    Like the oobasic build-in function msgbox,
    but simplified as only intended for quick debugging.
    Signature: msgbox(message, title='Message', boxtype='message', buttons=1, win=None).
    '''
    types = {'message': MESSAGEBOX, 'info': INFOBOX, 'error': ERRORBOX,
             'warning': WARNINGBOX, 'query': QUERYBOX}
    tk = createUnoService("com.sun.star.awt.Toolkit")
    if not win:
        desktop = createUnoService("com.sun.star.frame.Desktop")
        frame = desktop.ActiveFrame
        if frame.ActiveFrame:
            # top window is a subdocument
            frame = frame.ActiveFrame
        win = frame.ComponentWindow
    box = tk.createMessageBox(win, types[boxtype], buttons, title, message)
    return box.execute()

# -----------------------------------------------------------
#   MAIN
# -----------------------------------------------------------


def insertqrcode_int(doc, ctx, qrpath=None):
    # préparer les objets nécessaires
    tempfile = ctx.ServiceManager.createInstanceWithContext(
        "com.sun.star.io.TempFile", ctx)
    url = tempfile.Uri
    count = 1

    if doc.supportsService("com.sun.star.text.TextDocument"):
        """
        Traitement réalisé dans un document texte
        """
        # the writer controller impl supports the css.view.XSelectionSupplier interface
        xSelectionSupplier = doc.getCurrentController()
        xIndexAccess = xSelectionSupplier.getSelection()
        xTextRange = None
        try:
            xTextRange = xIndexAccess.getByIndex(0)
        except:
            return

        theString = xTextRange.getString()
        szString = len(theString)
        if szString == 0:
            return
        if szString > 300:
            msgbox("Le texte sélectionné comporte %d caractères" %
                   szString, title="Attention", boxtype='warning')

        xText = xTextRange.getText()  # get the XText interface

        # créer le qr code
        create_qrcode(url, theString)

        # créer un curseur pour l'insertion de l'image
        xcurs = xText.createTextCursorByRange(xTextRange)

        # créer l'image
        oShape = doc.createInstance(
            "com.sun.star.drawing.GraphicObjectShape")
        oShape.GraphicURL = url
        oShape.Title = 'qr'+str(count)
        sz = 3000 + len(theString)*4
        oShape.setSize(Size(sz, sz))
        count += 1
        oShape.Transparency = 0
        oShape.LineTransparence = 100

        # ces lignes sont à placer AVANT le "insertTextContent" pour que la forme soit bien configurée
        oShape.AnchorType = AT_PARAGRAPH  # com.sun.star.text.TextContentAnchorType
        oShape.HoriOrient = RIGHT  # com.sun.star.text.HoriOrientation
        oShape.LeftMargin = 10
        oShape.HoriOrientRelation = PRINT_AREA  # com.sun.star.text.RelOrientation
        oShape.VertOrient = TOP  # com.sun.star.text.VertOrientation
        oShape.VertOrientRelation = PRINT_AREA  # com.sun.star.text.RelOrientation
        # oShape.Surround = LEFT  # com.sun.star.text.WrapTextMode

        # insertion de la forme dans le texte à la position du curseur
        xText.insertTextContent(xcurs, oShape, False)

        # cette ligne est à placer APRÈS le "insertTextContent" qui écrase la propriété
        oShape.TextWrap = LEFT
    else:
        """
        Traitement réalisé dans une feuille de calcul
        """

        # récupération du document et du contexte
        oSheet = doc.getCurrentController().getActiveSheet()
        oPage = oSheet.getDrawPage()

        iRow = 1
        for iRow in range(100):
            for iCol in range(100):
                cell = oSheet.getCellByPosition(iCol, iRow)

                # remplace toutes les cellules qui ont une couleur de fond
                if cell.CellBackColor >= 0:
                    txt = cell.Text.getString()
                    if len(txt) > 0:
                        # enlever le texte de la cellule et enlever la couleur de fond
                        cell.Text.setString("")
                        cell.CellBackColor = -1

                        # créer l'image
                        create_qrcode(url, txt)

                        # insérer l'image dans la feuille
                        oShape = doc.createInstance(
                            "com.sun.star.drawing.GraphicObjectShape")
                        oShape.GraphicURL = url
                        oShape.Title = 'qr'+str(iCol)+str(iRow)
                        sz = min(cell.Size.Width, cell.Size.Height)
                        oShape.Size = Size(sz, sz)
                        oShape.FillTransparence = 100
                        oShape.LineTransparence = 100
                        oPage.add(oShape)
                        oShape.Anchor = cell


def insertqrcode(qrpath=None):
    insertqrcode_int(XSCRIPTCONTEXT.getDocument(),
                     XSCRIPTCONTEXT.getComponentContext())


class InsertQRCode(unohelper.Base, XJobExecutor):
    '''
    Point d'entrée principal du module
    '''

    def __init__(self, context):
        self.context = context

    def trigger(self, arg):
        smgr = self.context.getServiceManager()
        desktop = smgr.createInstanceWithContext(
            'com.sun.star.frame.Desktop', self.context)
        insertqrcode_int(desktop.getCurrentComponent(), self.context)


g_exportedScripts = insertqrcode,

g_ImplementationHelper.addImplementation(
    InsertQRCode, 'lire.libre.oqrcode.impl', ())
