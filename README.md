# OQRcode

OQRcode est une extension de LibreOffice ou d'Apache OpenOffice destinée à insérer des QR code simplement, soit dans le traitement de texte, soit dans une feuille de calcul. Ce code est construit à partir de PyQRCode (https://pypi.org/project/PyQRCode/).

## Installation

Télécharger le fichier de l'extension : https://framagit.org/arkaline/oqrcode/-/raw/main/dist/oqrcode.oxt

Intaller cette extension vie le gestionnaire des extensions.

## Utilisation

Dans le traitement de textes, sélectionner une portion de texte puis menu : "Insérer -> QR code"

Dans une feuille de calcul, indiquer les cellules dont le texte doit être transformé en QR code en leur donnant un fond de couleur puis menu : "Insérer -> QR code"

Point d'attention : lorsque la longueur du texte à transformer est supérieure à 300 caractères, un message averti de la longueur de ce texte. Le but est d'informer l'utilisateur qu'un texte trop long risque de produire un QR code trop complexe à réchiffrer.

## Auteur

Michael Nooner : https://github.com/mnooner256/pyqrcode
intégré comme extension dans LibreOffice par Marie Brungard

## License

voir le fichier LICENSE

## Statut

en test
